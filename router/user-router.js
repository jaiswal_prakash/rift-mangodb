const express = require('express');
const route = express.Router();
const UserDAO = require('../DAO/userDAO');

route.post('/', async function (req, res) {
    let data = await UserDAO.create(req.body);
    if (data) {
        res.status(200).send(data);
    }
    else {
        
        res.status(404).send('error');
    }

});

module.exports = route;