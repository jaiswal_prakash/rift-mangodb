const UserModel = require('../Model/User');

const UserDAO ={
    create:(bodyData)=>{
        return new UserModel({
            name:bodyData.name,
            email:bodyData.email,
            password:bodyData.password,
            contact:bodyData.contact,
            otp:123
        }).save();
    }
}

module.exports =UserDAO;