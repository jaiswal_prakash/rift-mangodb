var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupExpensesSchema = new Schema({
    groupId:{
        type:String,
        required: true
    },
    userId:{
        type:String,
        required: true
    },
    date:{
        type:String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true,

    },
    expId: {
        type: String,
        required: true,
        trim: true

    },

    status: {
        required: true,
        type: number

    },
});
module.exports = mongoose.module("groupExpenses", groupExpensesSchema);