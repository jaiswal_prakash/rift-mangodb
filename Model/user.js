var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true

    },
    contact: {
        type: Number,
        required: true
    },
    otp: {
        trim: true,
        type: Number

    },
});
module.exports = mongoose.model("user", userSchema);