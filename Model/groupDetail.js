var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupDetailSchema = new Schema({
    groupName: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,

    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    member:[ {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true

    }],
});
module.exports = mongoose.module("groupDetail", groupDetailSchema);