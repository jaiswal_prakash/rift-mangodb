var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var settleUpSchema = new Schema({
    date: {
        type: String,
        required: true
    },
    groupID: {
        type: Schema.Types.ObjectId,
        ref: 'groupDetail',
        required: true
    },
    userDetail: [{
        type: JSON,
        required: true,
        trim: true

    }],
    
});
module.exports = mongoose.module("settleUp", settleUpSchema);