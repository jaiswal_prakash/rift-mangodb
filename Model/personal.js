var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var personalSchema = new Schema({
    personalId:{
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    description: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true,

    },
    expId: {
        type: String,
        required: true,
        trim: true

    },
    status: {
        required: true,
        type: number

    },
});
module.exports = mongoose.module("personal", personalSchema);