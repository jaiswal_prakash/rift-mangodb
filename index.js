const express  = require('express');
var mongoose = require('mongoose');
const app = express();
const path = require('path');


//----------------bodyParser for post---------------//

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// -------connection for database-----------//
const mongoDB = 'mongodb://localhost/rift'; // create the mooc database 
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log('Mongodb connected successfully');
});
app.use(express.static(path.join(__dirname, 'views')));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');// to project the views folder

//  --------------routing-----------------------

app.use('/user', require('./router/user-router'));


// ----------- defining port number------------
app.listen(3000, () => {
    console.log("listening to the port 3000")
});